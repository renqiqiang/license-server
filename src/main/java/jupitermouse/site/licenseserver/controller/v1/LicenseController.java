package jupitermouse.site.licenseserver.controller.v1;

import jupitermouse.site.licenseserver.controller.dto.CustomLicenseParam;
import jupitermouse.site.licenseserver.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 许可证生成Controller
 * </p>
 * 
 * @author qiqiang.ren 2019/04/16 17:27
 */
@RestController("LicenseController.v1")
@RequestMapping("/license")
public class LicenseController {

    @Autowired
    private LicenseService service;
    
    @GetMapping
    public void creator(CustomLicenseParam param, HttpServletResponse response){
        service.generatorLicense(param, response);
    }

}
