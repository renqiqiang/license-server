package jupitermouse.site.licenseserver.controller.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import jupitermouse.site.license.LicenseCheckModel;
import lombok.*;

import java.util.Date;

/**
 * <p>
 * 自定义的参数
 * </p>
 *
 * @author qiqiang.ren 2019/04/16 16:32
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomLicenseParam {

    /**
     * 证书subject
     */
    private String subject;

    /**
     * 证书生效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date issuedTime = new Date();

    /**
     * 证书失效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expiryTime;

    /**
     * 额外的服务器硬件校验信息
     */
    private LicenseCheckModel extra;
}
