package jupitermouse.site.licenseserver.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 配置类
 * </p>
 * 
 * @author qiqiang.ren 2019/04/16 17:40
 */

@Data
@Component
@ConfigurationProperties(prefix = LicenseServerProperties.PROPERTY_PREFIX)
public class LicenseServerProperties {

    public static final String PROPERTY_PREFIX = "application.license";

    private Server server;

    private String licenseParentDir = "/";

    @Getter
    @Setter
    public static class Server{
        /**
         * 密钥别称
         */
        private String privateAlias;

        /**
         * 密钥密码（需要妥善保管，不能让使用者知道）
         */
        private String keyPass;

        /**
         * 访问秘钥库的密码
         */
        private String storePass;

        /**
         * 证书生成路径
         */
        private String licensePath;

        /**
         * 密钥库存储路径
         */
        private String privateKeysStorePath;

        /**
         * 用户类型
         */
        private final String consumerType = "user";

        /**
         * 用户数量
         */
        private final Integer consumerAmount = 1;

        /**
         * 描述信息
         */
        private final String description = "";

        private final String x500Principal = "CN=localhost, OU=localhost, O=localhost, L=SH, ST=SH, C=CN";
    }
}
