package jupitermouse.site.licenseserver.service;

import jupitermouse.site.licenseserver.controller.dto.CustomLicenseParam;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 许可证生成服务
 * </p>
 * 
 * @author qiqiang.ren 2019/04/16 16:36
 */
public interface LicenseService {

    /**
     * 生成许可证
     * @param param 自定义参数
     * @param response HttpServletResponse
     */
    void generatorLicense(CustomLicenseParam param, HttpServletResponse response);
}
