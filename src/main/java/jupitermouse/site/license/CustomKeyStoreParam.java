package jupitermouse.site.license;

import de.schlichtherle.license.AbstractKeyStoreParam;

import java.io.*;

/**
 * <p>
 * 存储库
 * </p>
 *
 * @author qiqiang.ren 2019/03/22 10:51
 */
public class CustomKeyStoreParam extends AbstractKeyStoreParam {

    /**
     * 公钥/私钥在磁盘上的存储路径
     */
    private String storePath;
    /**
     * key alias
     */
    private String alias;

    /**
     * 公钥密码
     */
    private String storePwd;

    /**
     * 私钥密码
     */
    private String keyPwd;

    public CustomKeyStoreParam(Class clazz, String resource, String alias, String storePwd, String keyPwd) {
        super(clazz, resource);
        this.storePath = resource;
        this.alias = alias;
        this.storePwd = storePwd;
        this.keyPwd = keyPwd;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    @Override
    public String getStorePwd() {
        return storePwd;
    }

    @Override
    public String getKeyPwd() {
        return keyPwd;
    }

    /**
     * 用于将公私钥存储文件存放到其他磁盘位置而不是项目中
     * @return InputStream
     * @throws IOException
     */
    @Override
    public InputStream getStream() throws IOException {
        return new FileInputStream(new File(storePath));
    }
}
